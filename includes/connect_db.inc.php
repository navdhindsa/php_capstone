<?php

/**
  * Capstone
  * @file connect_db.inc.php
  * @course  PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-09-11
  */

//configuration settings for database connections
define('DB_USER','root');
define('DB_PASS','root');
define('DB_DSN','mysql:host=localhost;dbname=capstone');

$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);