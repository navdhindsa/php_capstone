<!doctype html>
<html lang="en">
  <head>
    <title><?=$title?></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- favorites and apps icons -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- embedding Google font -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,700" rel="stylesheet">
 
    <!-- embedding stylesheets -->
    <link rel="stylesheet" type="text/css" href="styles/desktop.css" media="screen and (min-width:769px)" />
    <link rel="stylesheet" type="text/css" href="styles/mobile.css" media="screen and (max-width:768px)" />
    <link rel="stylesheet" type="text/css" href="styles/print.css" media="print" />
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
    <script src="scripts/headerchange.js"></script>
    <?php
    /**
      * Capstone
      * @file header.inc.php
      * @course Intro PHP, WDD 2018 Jan
      * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
      * @created_at 2018-08-02
   */
    ?>
    <style>
		
	</style>
    
    <!--
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||                                                            ||
||                                                            ||
||                                                            ||
||                .-.                                         ||
||       ,     .-.   .-.                                      ||
||      / \   (   )-(   )          WDD WDD Capstone Project   ||
||      \ |  ..>-( . )-<           By- Navdeep Kaur Dhindsa   ||
||       \|..'(   )-(   )          WDD PACE 2018              ||
||        Y ___`-'   `-'                                      ||
||        |/__/   `-'                                         ||
||        |                                                   ||
||        |                                                   ||
||        |                                                   ||
||  ______|________                                           || 
|| |_______________|                                          ||
||  \             /                                           ||
||   \           /                                            ||
||    \         /                                             ||
||     \       /                                              ||
||      \_____/                                               ||
||                                                            ||
||                                                            ||
||                                                            ||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||  
  
  
  
  
  -->
  
    <!-- conditions for IE9 and earlier support -->
    <!--[if LTE IE 9]>
      <link rel="stylesheet" type="text/css" href="styles/desktop.css" />
      <script>
        document.createElement('header');
        document.createElement('nav');
        document.createElement('footer');
        document.createElement('main');
        document.createElement('section');
      </script>

      <style type="text/css">
        header, nav, main, section, footer{
          display: block;
        }
      </style>
    <![endif]-->
    
    <!-- IEs to avoid borders on image links -->
    <style>
      a img{
        border: none;
        outline: none;
      }
    </style>
    
    
    <script>
      $(document).ready(function(){
      	$('#search_form input').keyup(function(e){

      		var keyword = {};
      		keyword.string = $('form #s').val();

      		$.post('../database/server.php', keyword, function(response){
					
					viewResults(response);
				});
      		
      	});
      	
      });

      function viewResults(response){
      	
      	var html = '<ul>';
      	//console.log(response[0].title);
      	for(var i=0; i<response.length; i++){
      		// html += '<li data-id="'+response[i].book_id+'">'+response[i].title+'</li>';
      		 html+='<li ><a  href="post.php?post_id='+response[i].post_id+'">'+response[i].title+'</a></li>';

      	}

      	html +='</ul>';
      	$('#search_box').html(html);
      }

    </script>
    
  </head>


