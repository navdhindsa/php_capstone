<?php
  /**
  * Capstone
  * @file footer.inc.php
  * @course Intro PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */
?>
      <div id="top">
        <a href="#" title="Go to Top"><img src="images/top.png" alt="Top"></a>
      </div>
</div><!-- wrapper ends-->
    
    
    <footer>
      <div id="top_row">
        <ul>
            <li><a href="index.php"   title="Home">HOME</a></li><!--Home-->
            <li><a href="about.php"    title="About">ABOUT</a></li><!--About-->
            <li><a href="categories.php"   title="Categories">CATEGORIES</a></li>
            <li><a href="post.php"    title="Posts">POSTS</a></li><!--Posts-->
            <li><a href="register.php"   title="Register">REGISTER</a></li><!--register-->
            <li><a href="contact.php"    title="Contact">CONTACT</a></li><!--contact-->
          </ul>
      </div>
      <div id="social_icons">
          <div class="icons"><a href="https://www.facebook.com"><img onmouseover="fbChanger();" onmouseout="fbOriginal();" id="fb" src="images/facebooks_icon.svg" alt="facebook"/></a></div>
          <div class="icons"><a href="https://www.instagram.com"><img onmouseover="igChanger();" onmouseout="igOriginal();" id="ig" src="images/instagram_icon.svg" alt="instagram"/></a></div>
          <div class="icons"><a href="https://www.twitter.com"><img onmouseover="twChanger();" onmouseout="twOriginal();" id="tw" src="images/twitter_icon.svg" alt="twitter"/></a></div>
          <div class="icons"><a href="https://www.linkedin.com"><img onmouseover="lkChanger();" onmouseout="lkOriginal();" id="lk" src="images/linkedin_icon.svg" alt="linkedin"/></a></div>
        </div>

        <p>
          Copyright &copy; 2018 Navdeep. All rights reserved.
        </p>
      
    </footer>
    

  </body>
</html>
