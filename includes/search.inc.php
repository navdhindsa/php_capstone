			<script>

				$(document).ready(function(){
					releasedKey();
				});
				
				function releasedKey(){
		      $('#search_form input').keyup(function(e){
		        e.preventDefault();
		        var data = {};
		        data.search = $('#s').val();
		        console.log(data.search);

		        searchTitle(data);
		      });
		    }
				
				function searchTitle(data){
					$.post('server/search_function.php', data, function(response){
			        console.log(response);
			        showListofItems(response);
			         });
				}

				function showListofItems(data){
				  var content = '<ul>';
		          for(var i=0; i<data.length; i++){
		        
		          content += '<li data-id="'+ data[i].blog_id +'">'+ data[i].title +'</li>';
		        }
		      content +='</ul>';
		      $('#list').html(content);

		      handleClicks();
				}

				function handleClicks(){
		      $('#list li').each(function(){
		        $(this).click(function(){
		          console.log($(this).attr('data-id'));
		          
		          location.href = 'post.php?post_id='+$(this).attr('data-id');
		        });
		      });
		    }
		    
			</script>
			<div class="wrap">
        <div class="search">
          <form id="search_form" autocomplete="off" novalidate >
            <input id="s" type="text" name="s" class="searchTerm" placeholder="What are you looking for?">
            <button type="submit" class="searchButton">
            &#128269;
            </button>
            <div id="list">

        		</div>
          </form>
        </div>
      </div>