
<?php
  /**
  * Capstone
  * @file nav.inc.php
  * @course Intro PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */
?>
  
      
       <header>
        <div id="top_bar">
        
        <div id="util">        
            
            <?php if(isset($_SESSION['logged_in'])) : ?>
              <a href="profile.php" title="PROFILE" ><img onmouseover="profileChanger();" onmouseout="profileOriginal();" id="profile" src="images/nav_profile.svg" alt="Profile" title="Profile" /> </a>
              <a href="library.php" title="LIBRARY" ><img onmouseover="libraryChanger();" onmouseout="libraryOriginal();" id="library" src="images/library.svg" alt="Library" title="Library" /> </a>
              <a href="checkout.php" title="CART" ><img onmouseover="cartChanger();" onmouseout="cartOriginal();" id="cart" src="images/cart.svg" alt="Cart" title="Cart" /> </a>
              <?php if(isset($_SESSION['is_admin'])) : ?>
              <a href="admin/index.php" title="ADMIN HOME" ><img onmouseover="adminChanger();" onmouseout="adminOriginal();" id="admin" src="images/admin.svg" alt="Admin" title="Admin" /> </a>
            <?php endif; ?>
              <a href="logout.php" title="LOGOUT"><img onmouseover="loChanger();" onmouseout="loOriginal();" id="logout" src="images/nav_logout.svg" alt="Logout" title="Logout" /></a>
            
              <?php else : ?>
              <a href="login.php" title="LOGIN"><img onmouseover="loginChanger();" onmouseout="loginOriginal();" id="login" src="images/nav_login.svg" alt="Login" title="Login" /></a>
              <a href="register.php" title="REGISTER"><img onmouseover="regChanger();" onmouseout="regOriginal();" id="reg" src="images/nav_register.svg" alt="Register" title="Register" /></a>
              <?php endif; ?>
              
          
          </div>
          <div id="tagline">
          <img src="images/navlogo.svg" alt="logo" />            
          </div>
          <div class= "nav2">
          <div id="menu2">
            <a href="#" id="menulink2" title="Menu">
              <div class="bar1"></div>
              <div class="bar2"></div>
              <div class="bar3"></div>
            </a>
             
            <ul id="navlist2">
              <li><a href="index.php"  <?php if($slug == 'index') {echo 'class="current"';}?> title="Home">HOME</a></li><!--Home-->
              <li><a href="about.php"   <?php if($slug == 'about') {echo 'class="current"';}?>  title="About">ABOUT</a></li><!--About-->
              <li><a href="categories.php"   <?php if($slug == 'categories') {echo 'class="current"';}?>  title="Categories">CATEGORIES</a></li><!--Categories-->
              <li><a href="posts.php"   <?php if($slug == 'post') {echo 'class="current"';}?>  title="Posts">POSTS</a></li><!--Posts-->
              <?php if(isset($_SESSION['logged_in'])) : ?>
                <li><a href="profile.php"   <?php if($slug == 'profile') {echo 'class="current"';}?>  title="Profile">PROFILE</a></li><!--register-->
              <?php else : ?>
                <li><a href="register.php"   <?php if($slug == 'register') {echo 'class="current"';}?>  title="Register">REGISTER</a></li><!--register-->
              <?php endif; ?>
              <li><a href="contact.php"   <?php if($slug == 'contact') {echo 'class="current"';}?>  title="Contact">CONTACT</a></li><!--contact-->
            </ul>
          </div>
        </div>
        
      </div> <!-- left header ends-->
</header>
        <div id="wrapper">
         
        <nav id= "nav">
          <div id="menu">
            <a href="#" id="menulink" title="Menu">
              <div class="bar1"></div>
              <div class="bar2"></div>
              <div class="bar3"></div>
            </a>
             
            <ul id="navlist">
              <li><a href="index.php"  <?php if($slug == 'index') {echo 'class="current"';}?> title="Home">HOME</a></li><!--Home-->
              <li><a href="about.php"   <?php if($slug == 'about') {echo 'class="current"';}?>  title="About">ABOUT</a></li><!--About-->
              <li><a href="categories.php"   <?php if($slug == 'categories') {echo 'class="current"';}?>  title="Categories">CATEGORIES</a></li><!--Categories-->
              <li><a href="posts.php"   <?php if($slug == 'post') {echo 'class="current"';}?>  title="Posts">POSTS</a></li><!--Posts-->
              <?php if(isset($_SESSION['logged_in'])) : ?>
                <li><a href="profile.php"   <?php if($slug == 'profile') {echo 'class="current"';}?>  title="Profile">PROFILE</a></li><!--register-->
              <?php else : ?>
                <li><a href="register.php"   <?php if($slug == 'register') {echo 'class="current"';}?>  title="Register">REGISTER</a></li><!--register-->
              <?php endif; ?>
              <li><a href="contact.php"   <?php if($slug == 'contact') {echo 'class="current"';}?>  title="Contact">CONTACT</a></li><!--contact-->
            </ul>
          </div>
        </nav>
   