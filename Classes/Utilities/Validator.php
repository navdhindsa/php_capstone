<?php

namespace Classes\Utilities;

class Validator
{
  private $errors = [];
  
   /**
   * Validate for required
   * @param String $field_name
   
   */
  public function required($field_name)
  {
      $label = ucfirst(str_replace('_', ' ', $field_name));
      if(empty($_POST[$field_name])) {
        $this->errors[$field_name] = "{$label} is a required field.";  
      }
     
  }
  
  /**
   * Validate for email
   * @param String $field_name
   
   */
  public function validEmail($field)
  {
    if(!filter_input(INPUT_POST, $field, FILTER_VALIDATE_EMAIL)) {
        $this->errors[$field] = "Please enter a valid email address";
        
    }
  }
  
  
  /**
* Escape a string for use ingeneral HTML
* @param String $string the string to be escaped
* @return String the escaped string
*/
public function esc($string) 
{
    return htmlspecialchars($string, NULL, 'UTF-8', false);
}

/**
* Escape a string for use in an attribute
* @param String $string the string to be escaped
* @return String the escaped string
*/
public function esc_attr($string)
{
    return htmlspecialchars($string, ENT_QUOTES, "UTF-8", false);
}

  
/**
 * Check for the validation of a general string
 * @param  String $field the field to be validated
 * @return Array $errors if the field is not valid it will return an error in the same name
 */
public function validString($field)
{
  if(!empty($_POST[$field])){
    if(preg_match("/^[A-z]+$/", $_POST[$field],$matches)===0){
      $this->errors[$field]="Please enter a valid $field";
    }
  }
}

/**
 * function to match the password with confirm password field
 * @return Array $errors if the passwords do not match it will return an error 
 */
public function passwordMatch()
{
  if(!empty($_POST['password']) && !empty($_POST['confirm_password'])){
    if($_POST['password'] !== $_POST['confirm_password']) {
      $this->errors['password'] = 'Passwords do not match';
    }
  }
}

/**
 * check for the valid phone number
 * @param  String $field The phone field 
 * @return Array $errors if the phone  is not valid it will return an error in the same name
 */
public function validPhone($field)
{
  if(!empty($_POST[$field])){
    preg_match("/^\(?(.{3})[\)\-\s]?(.{3})[\)\-\s]?(.{4})$/", $_POST[$field],$matches);
    
    if(!is_numeric($matches[1]) || !is_numeric($matches[2]) || !is_numeric($matches[3])){
      $this->errors[$field]= "Phone is not a number";
    }
  }
  elseif(preg_match("/^\(?([0-9]{3})[\)\-\s]?([0-9]{3})[\)\-\s]?([0-9]{4})$/", $_POST[$field],$matches)===0){
    $this->errors[$field]="Please enter the phone in correct format";
  }
}

/**
 * function to check the validation of age
 * @param  String $field the age field
 * @return Array $errors if age is not valid it will return an error in the same name
 */
public function validAge($field)
{
  $label = ucfirst(str_replace('_', ' ', $field));
  if(!empty($_POST[$field])){
    if(!is_numeric($_POST[$field])){
      $this->errors[$field]= $label . " is not a number";
  
      }
    }
}

/**
 * function to validate that a field has minimum length
 * @param  String $field the field to be validated for length
 * @param  Number $len the number for a minimum value
 * @return Array $errors if the field is not valid it will return an error in the same name
 */
public function minLength($field,$len)
{
  if(!empty($_POST[$field])){
    if( $_POST[$field]<$len){
      $this->errors[$field]= "Please don't be so modest!!";
    } 
  }
}

/**
 * function to validate that a field has maximum length
 * @param  String $field the field to be validated for length
 * @param  Number $len the number for a maximum value
 * @return Array $errors if the field is not valid it will return an error in the same name
 */
public function maxLength($field,$len)
{
  if(!empty($_POST[$field])){
    if( $_POST[$field]>$len){
     $this->errors[$field]= "We don't think you are that old!"; 
    } 
  }
}

/**
 * function to validate postal code with canadian rules
 * @param  String $field the field to be validated for postal code
 * @return Array $errors if the field is not valid it will return an error
 */
public function validPostCode($field)
{
  if(!empty($_POST[$field])){
    if(preg_match("/^[A-z][0-9][A-z]\s?[0-9][A-z][0-9]$/", $_POST[$field],$matches)===0){
      $this->errors['postal_code']="Please enter the postal code in correct format";
    }
  }
}


/**
 * function to validate the strength of password
 * @param  String $field the password field to be validated
 * @return Array $errors if the field is not valid it will return an error
 */
public function strongPass($field)
{
  if(!empty($_POST[$field])){
    if(preg_match("/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[[:punct:]]).{8,}/", $_POST[$field],$matches)===0){
      $this->errors[$field]="Your password is not strong enough.Please include a capital letter, small letter, number and a special character";
    }
  }
}

/*
public function isSet($field)
{
  $label = ucfirst(str_replace('_', ' ', $field));
  if(!($_POST[$field]==='0'||$_POST[$field]==='1')) {
    $this->errors[$field] = "{$label} is a required field.";  
  }
     
  }
*/
/**
 * function to return all the validation errors in an errors array
 * @return Array $errors  the errors array 
 */
public function errors()
{
  return $this->errors;
}

}