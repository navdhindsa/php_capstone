<?php
/**
  * Capstone
  * @file queries.php
  * @course PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-09-11
  */


/**
 * gets  author details
 * @param  Object $dbh, the database handler
 * @param  String $author_id to be worked on
 * @return Array, the details of posts by author
 */
function getAuthor($dbh, $author_id)
{
  $query = 'SELECT 
  blog.title AS title,
  users.first_name AS author_f,
  users.last_name AS author_l,
  
  blog.id AS blog_id,
  category.name AS category,
  
  blog.user_id AS user_id,
  blog.created_at AS date,
  
  blog.image AS image,
  blog.description AS description
  FROM blog
  JOIN users USING(user_id)
  JOIN category USING(category_id)
  WHERE user_id=:author_id AND is_deleted=0';
  
  $stmt =$dbh-> prepare($query);
  $params= array(':author_id'=>$author_id);
  $stmt -> execute($params);
  
  return  $stmt-> fetchAll(PDO::FETCH_ASSOC);
  
}

/**
 * Get random number of books for list view, if nothing is selected
 * @param  Object $dbh, the database handler
 * @param  Number $num_books, the number of books to fetch
 * @return Array the asked number of books in an arry
 */
function getAllPosts($dbh, $num_books)
{
  $query = 'SELECT 
  blog.title AS title,
  users.first_name AS author_f,
  users.last_name AS author_l,
  blog.content AS content,
  blog.id AS blog_id,
  category.name AS category,
  
  blog.user_id AS user_id,
  blog.created_at AS date,
  
  blog.image AS image,
  blog.description AS description
  FROM blog
  JOIN users USING(user_id)
  JOIN category USING(category_id)  WHERE is_deleted=0 LIMIT 5';
  
  $stmt =$dbh-> prepare($query);
  
  $stmt -> execute();
  
  $result =  $stmt-> fetchAll(PDO::FETCH_ASSOC);
  $random_keys = array_rand($result,$num_books);
  $random = array();
  
  foreach($random_keys AS $values){
    $random[$values] = $result[$values];
  }
  
  return $random;
  
}

/**
 * gets all the posts by a single author
 * @param  Object $dbh, the database handler
 * @param  String $author_id, the id of the author
 * @return Array a list of books by the author
 */
function getPostsAuth($dbh, $author_id)
{
  $query = 'SELECT 
  blog.title AS title,
  users.first_name AS author_f,
  users.last_name AS author_l,
  blog.content AS content,
  blog.id AS blog_id,
  category.name AS category,
  
  blog.user_id AS user_id,
  blog.created_at AS date,
  
  blog.image AS image,
  blog.description AS description
  FROM blog
  JOIN users USING(user_id)
  JOIN category USING(category_id)
  
  WHERE user_id = :author_id AND is_deleted=0
  ';
  
  $stmt =$dbh-> prepare($query);
  $params= array(':author_id'=>$author_id);
  $stmt -> execute();
  
  return  $stmt-> fetchAll(PDO::FETCH_ASSOC);
  
}


/**
 * gets the detail view for the blog post
 * @param  Object $dbh, the database handler
 * @param  String $post_id, the post id
 * @return Array the details of post
 */
function getPostDetail($dbh, $post_id)
{
  $query = 'SELECT 
  blog.title AS title,
  users.first_name AS author_f,
  users.last_name AS author_l,
  blog.content AS content,
  blog.id AS blog_id,
  category.name AS category,
  users.image as u_img,
  blog.thumb AS thumb,
  blog.user_id AS user_id,
  blog.created_at AS date,
  
  blog.image AS image,
  blog.description AS description
  FROM blog
  JOIN users USING(user_id)
  JOIN category USING(category_id)
  WHERE id=:book_id AND is_deleted=0';
  
  $stmt =$dbh-> prepare($query);
  $params= array(':book_id'=>$post_id);
  $stmt -> execute($params);
  
  return  $stmt-> fetch(PDO::FETCH_ASSOC);
 
}


/**
 * returns the post details if the title matches
 * @param  Object $dbh, the database handler
 * @param  String $keyword, the keyword to be searched
 * @return Array return the details of the post
 */
function searchPosts($dbh, $keyword)
{
  //var_dump($keyword);
  $query = "SELECT 
  blog.title AS title,
  users.first_name AS author_f,
  users.last_name AS author_l,
  category.name AS category,
  blog.id AS blog_id
  FROM blog
  JOIN users USING(user_id)
  JOIN category USING(category_id)
  WHERE blog.title = :keyword AND is_deleted=0
  ";
  
  //LIKE   CONCAT(:keyword, '%')
  $stmt =$dbh->prepare($query);
  $stmt->bindValue(':keyword',$keyword,PDO::PARAM_STR);
  $stmt -> execute();
  
  return  $stmt-> fetch(PDO::FETCH_ASSOC);
 
}


/**
 * search function for ajax search
 * @param  Object $dbh, the database handler
 * @param  String $keyword, the keyword is to be searched
 * @return Array an array of posts that match the title
 */
function searchPostsAjax($dbh, $keyword)
{
  $query = "SELECT 
  blog.title AS title,
  users.first_name AS author_f,
  users.last_name AS author_l,
  category.name AS category,
  blog.id AS blog_id,
  blog.user_id AS user_id,
  blog.created_at AS year,
  blog.thumb AS thumb
  FROM blog
  JOIN users USING(user_id)
  JOIN category USING(category_id)
  WHERE title LIKE   CONCAT(:keyword, '%') AND is_deleted=0 LIMIT 5
  ";
  
  $stmt =$dbh-> prepare($query);
  $params= array(':keyword'=>$keyword);
  $stmt -> execute($params);
  
  return  $stmt-> fetchAll(PDO::FETCH_ASSOC);
 
}

/**
 * function to get all category names for list view
 * @param  Object $dbh, the database handler
 * @return Array a list of category names with id
 */
function getCats($dbh)
{
  $query = 'SELECT  category.name AS name , category.category_id As cat_id, 
  category.image As image FROM category ';
  
  $stmt =$dbh-> prepare($query);
  
  $stmt -> execute();
  
  return $stmt-> fetchAll(PDO::FETCH_ASSOC);
}

/**
 * function te return the posts from a particluar category
 * @param  Object $dbh, the database handler
 * @param  String $category_id, the category id to be searched for
 * @return Array A list of all posts from the same category
 */
function getPostsCat($dbh, $category_id)
{
  $query = 'SELECT 
  blog.title AS title,
  users.first_name AS author_f,
  users.last_name AS author_l,
  category.name AS category,
  
  blog.id AS blog_id,
  blog.user_id AS user_id,
  
  blog.created_at AS year,
  
  blog.thumb AS thumb
  FROM blog
  JOIN users USING(user_id)
  JOIN category USING(category_id)
  WHERE category_id=:category_id AND is_deleted=0';
  
  $stmt =$dbh-> prepare($query);
  $params= array(':category_id'=>$category_id);
  $stmt -> execute($params);
  
  return  $stmt-> fetchAll(PDO::FETCH_ASSOC);
   
}

/**To get the details of profile
 * @param  [object] $dbh  [database handling object]
 * @param  [String] $id [The user id of user]
 * @return [Array]
 */
function getProfile($dbh, $id)
{

  //the query to select user data for profile
  $query = 'SELECT first_name,last_name,age,
      street,city,province,postal_code,country,
      email,phone 
            FROM users
            WHERE user_id = :id';
  $stmt = $dbh->prepare($query);

  $stmt->bindValue(':id', $id, PDO::PARAM_INT);

  $stmt->execute();

  return $stmt->fetch(PDO::FETCH_ASSOC);
}

/**To edit a blog
 * @param  [object] $dbh  [database handling object]
 * @param  [Array] $post [values of $_POST superglobal]
 * @return [Array]
 */
function editProfile($dbh, $post,$id)
{
  

  $query = 'Update  users set
                first_name = :first_name,
                last_name = :last_name, 
                age = :age, 
                street = :street, 
                city = :city, 
                province = :province, 
                postal_code = :postal_code, 
                country  = :country, 
                email = :email,
                phone= :phone
                WHERE user_id = :id

              ';

    $stmt = $dbh->prepare($query);

    $params = array (
                ':first_name' => $post['first_name'],
                ':last_name'=> $post['last_name'], 
                ':age'=> $post['age'], 
                ':street'=> $post['street'], 
                ':city'=> $post['city'], 
                ':province'=> $post['province'], 
                ':postal_code'=> $post['postal_code'], 
                ':country'=> $post['country'], 
                ':email'=> $post['email'],
                ':phone'=> $post['phone'],
                ':id' => $id
                
                );
    if($stmt->execute($params)) {
        //if the data is inserted successfully, set the session variables and go to profile page
        $_SESSION['profile_update_success'] = true;
        $_SESSION['profile_update_msg'] = 'Congratulations, you have successfully Updated Your Profile!!';
        return true;
        } // end if stmt 
}


function addToLibrary($dbh, $blog_id, $user_id)
{
  $query = 'INSERT INTO library 
              (blog_id, user_id, created_at)
              VALUES
              (:blog_id, :user_id, current_timestamp())';

      $stmt = $dbh->prepare($query);

      $params = array (
                ':blog_id' => $blog_id,
                ':user_id' => $user_id
                 );
      if($stmt->execute($params)) {
        //if the data is inserted successfully, set the session variables and go to profile page
        $_SESSION['library_success'] = true;
        $_SESSION['library_msg'] = 'Congratulations, you have successfully Added items to your library!!';
        return true;
        } // end if stmt
}

/**
 * function te return the posts from a particluar user's library
 * @param  Object $dbh, the database handler
 * @param  String $category_id, the category id to be searched for
 * @return Array A list of all posts from the same category
 */
function libraryPosts($dbh, $user_id)
{
  $query = 'SELECT blog_id from library WHERE user_id=:user_id';
  
  $stmt =$dbh-> prepare($query);
  $params= array(':user_id'=>$user_id);
  $stmt -> execute($params);
  
  return  $stmt-> fetchAll(PDO::FETCH_ASSOC);
   
}


/**To edit a blog
 * @param  [object] $dbh  [database handling object]
 * @param  [Array] $post [values of $_POST superglobal]
 * @return [Array]
 */
function editPassword($dbh, $post,$id)
{
  

  $query = 'Update  users set
                password = :password
                WHERE user_id = :id

              ';

    $stmt = $dbh->prepare($query);

    $params = array (
                ':password'=> password_hash($_POST['password'],PASSWORD_DEFAULT), 
                
                ':id' => $id
                
                );
    if($stmt->execute($params)) {
        //if the data is inserted successfully, set the session variables and go to profile page
        $_SESSION['profile_update_success'] = true;
        $_SESSION['profile_update_msg'] = 'Congratulations, you have successfully Updated Your Password!!';
        return true;
        } // end if stmt 
}


