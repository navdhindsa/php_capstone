<?php
/**
  * Capstone
  * @file server.php
  * @course PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-09-11
  */

//including the required files
include '../config/config.php';
include '../database/queries.php';

//If the server request methos id post, return the searched string with query
if($_SERVER['REQUEST_METHOD']=='POST'){
	//print_r($_POST['string']);
	if(!empty($_POST['string'])){
	$books = searchPostsAjax($dbh, $_POST['string']);
	
	header('Content-type: application/json');
	echo json_encode($books);
}
}

