<?php

//config
/**
      * Capstone
      * @file config.php
      * @course Intermediate PHP, WDD 2018 Jan
      * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
      * @created_at 2018-08-02
   */

//Starting sesion and buffering(output) is enable.
session_start();
ob_start();

//to check for the csrf token
if(empty($_SESSION['csrf'])){
  $_SESSION['csrf']=md5(time());
}

//put it into a variable
$csrf=$_SESSION['csrf'];

ini_set('display_errors',1);
ini_set('error_reporting', E_ALL);

//base path(current directory)
define('APP', __DIR__);




function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    require $fileName;
}
spl_autoload_register('autoload');

