<?php 
  /**
  * Capstone
  * @file about.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */
$title = 'Navdeep - About';
$slug = 'about';
 
//including the required files 
require __DIR__ . '/../config.php';

//the validation class inclusion
use \Classes\Utilities\Validator;
$v= new Validator();

//including the header
include '../includes/header.inc.php'; 

?>
  <body id="about">
    <?php include '../includes/nav.inc.php' ?>  
      <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->
        <h2>This is the about page</h2>
        <p>NKD Inc. is one of the few IT system integration, professional service and software development companies in Manitoba that works with Enterprise systems and companies. </p>
        <p>As a privately owned company, we provide IT Consultancy, software design and development as well as professional services and hardware deployment and maintenance.</p>
        <h3>Team members</h3>
        <p>We have an experienced and well qualified team working with us to make the dreams come true of our partners. </p>
        <p>We have been providing custom software and web development service from last 10 years.</p>
        <p> We have developed more than 100 products to more than 50 customers all over Canada and USA.</p>
        <p>Additionally, NKD is experienced in delivery, installation and support of our products and we have a special dedicated cell for grievance addressing.</p>
      </div>  
      <?php include '../includes/footer.inc.php' ?>
