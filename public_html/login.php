<?php 
/**
  * Capstone
  * @file login.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

$title = 'Login';
$slug = 'login';
require __DIR__ . '/../config.php';
require '../includes/connect_db.inc.php';



use \Classes\Utilities\Validator;
$v = new Validator();

//if the person has logged out, check for the session variable
if(isset($_SESSION['logout'])) {
	if(isset($_SESSION['logout_msg'])){
    //if the message is set, display it and unset it so that it dissapears on updating
    $msg=$_SESSION['logout_msg'];
    unset($_SESSION['logout_msg']);
    unset($_SESSION['logout']);
    session_regenerate_id();
   }
}

//if the person has logged out, check for the session variable
if(isset($_SESSION['not_admin'])) {
  if(isset($_SESSION['not_admin_msg'])){
    //if the message is set, display it and unset it so that it dissapears on updating
    $msg=$_SESSION['not_admin_msg'];
    unset($_SESSION['not_admin_msg']);
    unset($_SESSION['not_admin']);
    session_regenerate_id();
   }
}

//if the person has entered the email and password to login
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  //check for validation errors in email and password
  ////csrf checking
    if($_POST['csrf'] != $_SESSION['csrf']){
      die('You have not submitted the form from our website!');

    }//end csrf checking
  $v->required('email');
  $v->required('password');
  $errors = $v->errors();
  
  //if there are no errors
  if(empty($errors)){
    //query to fetch email and password
    $query = 'SELECT * FROM users
              WHERE email = :email';
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
    $stmt->execute();
    $users = $stmt->fetch(PDO::FETCH_ASSOC);
        
           
  //verify the password using password_verify(), if invalid then error
	if(!password_verify($_POST['password'] , $users['password'])) {
		$msg='You might wanna check your username and password!';
	} 


  else {
    //else set the session variables to log in
	  $_SESSION['logged_in'] = true;
    $_SESSION['id'] = $users['user_id'];
    $_SESSION['msg'] = "Welcome to your profile {$users['first_name']}";
    if($users['is_admin']=='1'){
    
    $_SESSION['is_admin']=true;
    $_SESSION['admin_msg']='You can find the link to admin page in the utility navigation above!';
  }
		header('Location: profile.php');
		die;
	}

}
}


//including the header file
include '../includes/header.inc.php';
?>
  <body id="connect">
   <?php include '../includes/nav.inc.php' ?>    
          <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->

          <h1>Login with your details</h1>
          <?php if(isset($msg)) : ?>

            <div class="fail"><?=$v->esc_attr($msg);?></div>
            
          <?php endif; ?>

            <div id="login">
            <form id="login" class="formEl" 
              method="post"
              action="login.php"
              name="login"
              autocomplete="on" novalidate>
          <fieldset>
            <legend>Register</legend>
            <input type="hidden" name="csrf" value="<?=$csrf?>" />
            <p>
              <label for="email">Email</label>
              <input type="text"
                id="email" 
                name="email" 
                maxlength="25"
                size="30"
                placeholder="Email" value="<?php if(!empty($_POST['email'])) 
                    echo $v->esc_attr($_POST['email']); ?>"
                />
            </p>
            <?php  if(!empty($errors['email'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['email']);?></span>
             <?php endif; ?>
             <p>
              <label for="password">Password</label>
              <input type="password"
                id="password" 
                name="password" 
                maxlength="25"
                size="30"
                placeholder="Password" 
                />
            </p>
            <?php  if(!empty($errors['password'])) : ?>
            <span class="error"><?=$errors['password']?></span>
             <?php endif; ?>
            <?php if(!empty($errors['login'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['login'])?></span>
             <?php endif; ?>
       
                       <p>
              <input type="submit" 
                     name="submit" 
                     id="submit" 
                     value="Submit" 
              />
              &nbsp;&nbsp;
              <input type="reset" 
                     name="reset" 
                     id="reset" 
                     value="Reset" 
              />
              </p>

          </fieldset>
        </form>
            <p>If you have forgotten your password, please submit your information on the contact page, we will get back to you!</p>
            
            
            </div>


    </div>

















 <?php include '../includes/footer.inc.php' ?>