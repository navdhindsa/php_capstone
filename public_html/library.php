<?php 
/**
  * Capstone
  * @file post.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

$title = 'Post';
$slug = 'post';
 
require __DIR__ . '/../config.php';
require '../includes/connect_db.inc.php';

include __DIR__ . '/../database/queries.php';

//if the person has added items to library, check for the session variable
if(isset($_SESSION['library_success'])) {
  if(isset($_SESSION['library_msg'])){
    //if the message is set, display it and unset it so that it dissapears on updating
    $msg=$_SESSION['library_msg'];
    unset($_SESSION['library_msg']);
    unset($_SESSION['library_success']);
    session_regenerate_id();
   }
}


$id= $_SESSION['id'];

$ids= libraryPosts($dbh, $id);

$item=[];

for($i=0; $i<count($ids); $i++){

  $item[$i]= getPostDetail($dbh, $ids[$i]['blog_id']); 
}

$posts= $item;




include '../includes/header.inc.php'; 
  
?>
  <body id="post">
   <?php include '../includes/nav.inc.php' ?>    
    
      <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->

        <!-- Main content starts-->  
      
        
        
       
<?php if(isset($msg)) : ?>

            <div class="success"><?=$msg?></div>
            <?php endif; ?>
<div id="home_content">
     <?php if(!empty($_SESSION['cart'])) : ?>
          <div class="cart_box">
           <p>Your Cart:</p>
            <ul>
              
              <?php foreach($_SESSION['cart'] AS $row) : ?>
                <li>
                  
                  <?=$row['title'];?> &nbsp; &nbsp;&nbsp;
                  
                  <a href="removeItem.php?id=<?=$row['id']?>">X</a>
                </li>

              <?php endforeach; ?>

            </ul>
            <a  href="checkout.php">Checkout</a>
          </div>

        <?php endif; ?>
        
            <div id="container">
              <h1>Your Library</h1>
              <?php foreach($posts as $row) : ?>
                <div class="home_cat">
                  <div class="home_img"><img  src="images/posts/<?=$row['thumb']?>.jpg" alt="<?=$row['title'];?>" />
                  </div>
                  <h4><?=$row['title']?></h4>
                  <h4><?=$row['category']?></h4>
                  <p>By: <?=$row['author_f'] . " " .$row['author_l']?></p>
                  
                  <div class="outer_a"><a class="button" href="post.php?post_id=<?=$row['blog_id']?>">Read</a>
           

                  <a class="button" href="">Delete</a></div>
            

                </div>
              <?php endforeach; ?>
            </div><!--container div ends -->
          
  
    
        
        
        
        
      
      </div>

    <?php include '../includes/footer.inc.php' ?>

