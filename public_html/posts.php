<?php 
/**
  * Capstone
  * @file post.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

$title = 'Post';
$slug = 'post';
 
//including required files 
require __DIR__ . '/../config.php';
require '../includes/connect_db.inc.php';
include __DIR__ . '/../database/queries.php';

//including the validator class
use \Classes\Utilities\Validator;
$v= new Validator();

//flag variable for search conditions
$condition = "regular";

//function to get category for list view and random 5 posts
$cat= getCats($dbh);
$posts = getAllPosts($dbh,5);

//if user search category wise, get the category id and call function to get posts by category
if(isset($_GET['category_id'])){
  $category_id=$_GET['category_id'];
  $posts = getPostsCat($dbh, $category_id);
}

//if the person has added item to cart , check for the session variable
if(isset($_SESSION['addItem'])) {
  if(isset($_SESSION['addItemMsg'])){
    //if the message is set, display it and unset it so that it dissapears on updating
    $msg=$_SESSION['addItemMsg'];
    unset($_SESSION['addItemMsg']);
    unset($_SESSION['addItem']);
    session_regenerate_id();
   }
}

//if the person has removed item from cart, check for the session variable
if(isset($_SESSION['removeItem'])) {
  if(isset($_SESSION['removeItemMsg'])){
    //if the message is set, display it and unset it so that it dissapears on updating
    $msg=$_SESSION['removeItemMsg'];
    unset($_SESSION['removeItemMsg']);
    unset($_SESSION['removeItem']);
    session_regenerate_id();
   }
}

//if user searches for a title, get the keyword and call the function to search posts

if(isset($_GET['s'])){
  $condition = "search";
  $keyword=$_GET['s'];
  //var_dump($_GET['s']);
  $search_result = searchPosts($dbh, $_GET['s']);
  //var_dump($search_result);
}

//include the header 
include '../includes/header.inc.php'; 
  
?>
  <body id="post">
   <?php include '../includes/nav.inc.php' ?>    
    
      <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->

        <!-- Main content starts-->  
        <div id="hero"> 

          <?php if($title == "Post"){
            include '../includes/search.inc.php';
          } ?>      
          <!-- 
          <div class="wrap">
            <div class="search">
              <form id="search_form" action="" method="" autocomplete="off" novalidate >
                <input id="s" type="text" name="s" class="searchTerm" placeholder="What are you looking for?">
                <button type="submit" class="searchButton">
                &#128269;
                </button>
              </form>
            </div>
          </div>
          -->
        </div>
        <?php if(isset($msg)) : ?>

            <div class="success"><?=$v->esc_attr($msg);?></div>
            <?php endif; ?>
        <?php if(!empty($_SESSION['cart'])) : ?>
          <div class="cart_box">
           <p>Your Cart:</p>
            <ul>
              
              <?php foreach($_SESSION['cart'] AS $row) : ?>
                <li>
                  
                  <?=$v->esc_attr($row['title']);?> &nbsp; &nbsp;&nbsp;
                  
                  <a href="removeItem.php?id=<?=$v->esc_attr($row['id'])?>">X</a>
                </li>

              <?php endforeach; ?>

            </ul>
            <a  href="checkout.php">Checkout</a>
          </div>

        <?php endif; ?>
        <div class="categories">

          <h3>Categories</h3>
          <ul id="cat_ul">
            <?php foreach($cat AS $row) : ?>
               
              <li><a href="posts.php?category_id=<?=$v->esc_attr($row['cat_id']);?>"><?=$v->esc_attr($row['name'])?></a></li>
          
            <?php endforeach;?>
          </ul>
              
        
          <?php if(!empty($_GET['category_id'])) : ?>
          
            <h2><?=$posts[0]['category']?></h2>
            <div id="cat_img"><img src="images/cats/<?=$v->esc_attr($_GET['category_id']);?>.jpg" alt="<?=$v->esc_attr($posts[0]['category'])?>" /></div>
                  
          <?php endif; ?>
              
          <?php if($condition == "regular") :?>    
            <div id="container">
              <?php foreach($posts as $row) : ?>
    		        <div class="home_cat home_cat_new">
                  <div class="home_img"><img  src="images/posts/<?=$row['blog_id']?>.jpg" alt="<?=$v->esc_attr($row['title']);?>" />
                  </div>
                  <h4><?=$v->esc_attr($row['title']);?></h4>
                  <h4><?=$v->esc_attr($row['category'])?></h4>
                  <p>By: <?=$v->esc_attr($row['author_f']) . " " .$v->esc_attr($row['author_l'])?></p>
                  
                  <div class="outer_a"><a class="button" href="post.php?post_id=<?=$v->esc_attr($row['blog_id'])?>">Read</a>
           

                  <a class="button" href="addToCart.php?post_id=<?=$v->esc_attr($row['blog_id'])?>">Save</a></div>
            

                </div>
              <?php endforeach; ?>
            </div><!--container div ends -->
          <?php endif; ?>

          <?php if($condition == "search") :?> 
            <?php if(!empty($search_result)) :?>   
              <div id="container">
                  <div class="home_cat">
                    <div class="home_img"><img  src="images/thumb/thumb.jpg" alt="<?=$v->esc_attr($search_result['title']);?>" />
                    </div>
                    <h4><?=$v->esc_attr($search_result['title'])?></h4>
                    <h4><?=$v->esc_attr($search_result['category'])?></h4>
                    <p>By: <?=$v->esc_attr($search_result['author_f']) . " " .$v->esc_attr($search_result['author_l'])?></p>
                    <div class="read_more"><a href="post.php?post_id=<?=$v->esc_attr($search_result['blog_id'])?>" >Read More</a></div>
                  
                  </div>
              </div><!--container div ends -->

            <?php else: ?>
              <p>Sorry, no books found !!</p>
            <?php endif; ?>
            <div id="clearfix"></div>
	        <?php endif;?>
          <?php if(empty($posts)) : ?>      
        
            <p>Sorry, no books found !!</p>
      
          <?php endif; ?>
        </div>
      </div>
      
      <?php include '../includes/footer.inc.php' ?>

