<?php 
 /**
  * Capstone
  * @file categories.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

$title = 'Categories';
$slug = 'categories';

//including the required files
require __DIR__ . '/../config.php';

require '../includes/connect_db.inc.php';
include __DIR__ . '/../database/queries.php';

//including the validator class
use \Classes\Utilities\Validator;
$v= new Classes\Utilities\Validator();


$cat= getCats($dbh);
//including the header
include '../includes/header.inc.php'; 
 
?>
  <body id="categories">
   <?php include '../includes/nav.inc.php' ?>    
         <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->
        <h2>Categories</h2>
      
         <div id="home_content">
          <?php foreach($cat AS $row) : ?>
               
              
          
            
          <div class="home_cat home_cat2 home_cat_new">
            <div class="home_img home_img2">
              <img src="images/cats/<?=$v->esc_attr($row['cat_id']);?>.jpg">
            </div>
            <h4><?=$v->esc_attr($row['name']);?></h4>
            
            
            
            
            <p><a class="button" href="posts.php?category_id=<?=$v->esc_attr($row['cat_id']);?>">See Posts</a></p>
            
          </div>

          <?php endforeach;?>
           
         
         
         
          
      </div><!-- Content Ends-->
      <?php include '../includes/footer.inc.php' ?>