<?php
/**
  * Capstone
  * @file delete.php
  * @course  PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-09-12
  */

  //including the required files 
  require '../../config.php';
  require '../../includes/connect_db.inc.php';
  include 'models/queries.php';

  //get the blog id from the url
  $id= $_GET['blog_id'];

  //query to set is_deleted field to 1 of the required item
  $query = 'Update  blog set
                is_deleted = 1,
                updated_at= current_timestamp()
                WHERE id = :id
              ';
  $stmt = $dbh->prepare($query);
  $params = array (
            ':id' => $id
            );
  if($stmt->execute($params)) {
    //if the data is inserted successfully, set the session variables and go to profile page
    $_SESSION['delete_success'] = true;
    $_SESSION['delete_msg'] = 'Congratulations, you have successfully deleted a blog!!';
    header('Location:detail.php?table=blog');
    die;
}