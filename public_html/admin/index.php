<?php
/**
  * Capstone
  * @file detail.php
  * @course  PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-09-12
  */
 
//including the required files 
require '../../config.php';
require '../../includes/connect_db.inc.php';
include 'models/queries.php';

//including the validator class
use \Classes\Utilities\Validator;
$v = new Validator();

//if the non admin user comes to this page, he is redirected to login page
if(!isset($_SESSION['is_admin'])){
  $_SESSION['not_admin']=true;
  $_SESSION['not_admin_msg']="You need to login with a privileged account to access that feature!";
  header('Location: ../login.php');
}
//getting aggregate data from blog table
$blog_info=getBlogData($dbh);

//getting aggregate data from user table
$user_info=getUserData($dbh);



 ?><!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="utf-8">
  <title>Admin Home</title>
  
  <link rel="stylesheet" type="text/css" href="styles/admin_styles.css" />
  
</head>
<body>
  <h1>Welcome Admin!</h1>
  <!--Links to various pages-->
  <div>
    <p class="util_links"><a href="index.php">Admin Home</a></p>
    <p class="util_links"><a href="../index.php">Website Home</a></p>
  </div>
  
  <!--  output list in html-->
  <div id="list">
    <ul>
      <li><a href="detail.php?table=blog">Blog</a></li>
      <li><a href="detail.php?table=users">Users</a></li>
      <li><a href="detail.php?table=library">Library</a></li>
      <li><a href="detail.php?table=comments">Comments</a></li>
    </ul>



  </div>
  <!--  if single book output detail-->
  <div id="detail">
    <p>Total Number of users: <span class="agg"><?=$v->esc_attr($user_info['id']);?></span></p> 
    <p>Total Number of blogs: <span class="agg"><?=$v->esc_attr($blog_info['id']);?></span></p> 
    <p>Average Number of Likes: <span class="agg"><?=$v->esc_attr($blog_info['likes']);?></span></p> 
    <p>Maximun Number of Likes: <span class="agg"><?=$v->esc_attr($blog_info['max']);?></span></p> 
    <p>Minimum Number of Likes: <span class="agg"><?=$v->esc_attr($blog_info['min']);?></span></p> 
   
  </div>
</body>
</html>