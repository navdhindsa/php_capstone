<?php
/**
  * Capstone
  * @file detail.php
  * @course  PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-09-12
  */


//including the required files 
require '../../config.php';
require '../../includes/connect_db.inc.php';
include 'models/queries.php';
//including the validator class
use \Classes\Utilities\Validator;
$v = new Validator();


//if the non admin user comes to this page, he is redirected to login page
if(!isset($_SESSION['is_admin'])){
  $_SESSION['not_admin']=true;
  $_SESSION['not_admin_msg']="You need to login with a privileged account to access that feature!";
  header('Location: ../login.php');
}
//if the table name is not in get, set it to null
if(!isset($_GET['table'])){
  $_GET['table']=null;
}

//if the server request method is get
if($_SERVER['REQUEST_METHOD']=='GET'){
  //if the table name is not empty call the function to display details of that table
  if(!empty($_GET['table'])){
    $tablename=$_GET['table'];
    $details=getTable($dbh,$tablename);
  }
  //if the search keyword is not empty call the function to display details of the related blogs
  if(!empty($_GET['search'])){
    $search=$_GET['search'];
    $details=searchTerms($dbh,$search);
  }
}


//checking the session variables to display flash messages on different actions
//to check for the creation of new blog
if(isset($_SESSION['create_success'])) {
  $msg = flashMessage('create_success','create_msg');
}

//to check for the updation of any blog
if(isset($_SESSION['update_success'])) {
  $msg = flashMessage('update_success','update_msg');
}

//to check for the deletion of any blog
if(isset($_SESSION['delete_success'])) {
  $msg = flashMessage('delete_success','delete_msg');
}

//to check for restore message
if(isset($_SESSION['restore_success'])) {
  $msg = flashMessage('restore_success','restore_msg');
}


?><!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="utf-8">
  <title>Details page</title>
  
  <link rel="stylesheet" type="text/css" href="styles/admin_styles.css" />
 
</head>
<body>
  <h1>Welcome Admin!</h1>
  <!--Links to various pages-->
  <div>
    <p class="util_links"><a href="index.php">Admin Home</a></p>
    <p class="util_links"><a href="../index.php">Website Home</a></p>
  </div>
   
  
  <!--  output list in html-->
  <div id="list">
    <ul>
      <li><a href="detail.php?table=blog">Blog</a></li>
      <li><a href="detail.php?table=users">Users</a></li>
      <li><a href="detail.php?table=library">Library</a></li>
      <li><a href="detail.php?table=comments">Comments</a></li>
    </ul>



  </div>
  <!--  if single book output detail-->
  <div id="detail">
    <?php if(isset($msg)) : ?>

      <div class="flash"><?=$msg?></div>
            
    <?php endif; ?>

    <?php if($_GET['table']=='blog'||isset($_GET['search'])) : ?>

      <form id="search" method="GET"><input  name="search" id="search_text" type="text" /><input type="submit" id="search_icon" value="Find" /></form>
      <p class="link"><a href="create_blog.php" >Create New Blog</a></p>
      
      <table> 

        <caption>Blog</caption>
        <tr>
          <th>Blog Id</th>
          <th>Blog Title</th>
          <th>Posted By</th>
          <th>Date and Time posted</th>
          <th>Category</th>
          <th>Deleted?</th>
          <th>Updated At</th>
          <th>Edit</th>
          <th>Delete / Restore</th>
          
        </tr>
        <?php foreach($details AS $row) : ?>
          <tr>
            <td><?=$v->esc_attr($row['id'])?></td>
            <td><?=$v->esc_attr($row['title'])?></td>
            <td><?=$v->esc_attr($row['author_f'])?> <?=$v->esc_attr($row['author_l'])?></td>
            <td><?=$v->esc_attr($row['post_date'])?></td>
            <td><?=$v->esc_attr($row['category'])?></td>
            <td><?=$v->esc_attr($row['is_deleted'])?></td>
            <td><?=$v->esc_attr($row['updated_at'])?></td>
            <td><a href="edit_blog.php?blog_id=<?$v->esc_attr(=$row['id'])?>" >Edit</a></td>
            <td>
              <?php if($row['is_deleted']==0) : ?>
                  <a href="delete.php?blog_id=<?=$row['id']?>" >Delete</a>
                <?php elseif ($row['is_deleted']==1) : ?>
                  <a href="restore.php?blog_id=<?=$row['id']?>" >Restore</a>
              <?php endif; ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    <?php else : ?>

      <p>Sorry, this functionality is not enabled yet!!</p>


    <?php endif; ?>
  </div>
</body>
</html>