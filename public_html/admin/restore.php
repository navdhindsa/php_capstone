<?php
/**
  * Capstone
  * @file restore.php
  * @course  PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-09-12
  */


//including the required files 
require '../../config.php';
require '../../includes/connect_db.inc.php';
include 'models/queries.php';

//get the blog id from the url
$id= $_GET['blog_id'];

//call the function to restore the blog
restoreBlog($dbh,$id);