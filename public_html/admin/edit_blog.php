<?php
/**
  * Capstone
  * @file detail.php
  * @course  PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-09-12
  */
 
//including the required files 
require '../../config.php';
require '../../includes/connect_db.inc.php';
include 'models/queries.php';

//if the non admin user comes to this page, he is redirected to login page
if(!isset($_SESSION['is_admin'])){
  $_SESSION['not_admin']=true;
  $_SESSION['not_admin_msg']="You need to login with a privileged account to access that feature!";
  header('Location: ../login.php');
}

//including the validator class
use \Classes\Utilities\Validator;
$v = new Validator();

$id = $_GET['blog_id'];

$fields = getFields($dbh,$id);

//if the admin has submitted data, check for errors from validation class
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  $fields=$_POST;
  $v->required('title');
  $v->required('post_id');
  $v->required('description');
  $v->required('content');
  //$v->isSet('status_check');
  $v->required('category_id');
  $v->required('user_id');
  $v->required('post_date');
 // $v->isSet('is_deleted');
  

  //make a list of all errors in an array
  $errors = $v->errors();
  //if there are no errors, insert into users table in the try block
  if(empty($errors )) {
    try{
      
      editBlog($dbh,$_POST);
    }//end try block

    //if there is an exception for not having a unique post_id, 
    //then catch it and give the required error message
    catch(PDOException $e){
      if(!empty($e->getMessage())){
        $errors['post_id']='This post_id is already taken, please use another??';
      }
    }
  } // end if no errors
} // end if POST submission


 ?><!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="utf-8">
  <title>Edit the post</title>
  
  <link rel="stylesheet" type="text/css" href="styles/admin_styles.css" />
  
</head>
<body>
  <h1>Welcome Admin!</h1>
  <!--Links to various pages-->
  <div>
    <p class="util_links"><a href="index.php">Admin Home</a></p>
    <p class="util_links"><a href="../index.php">Website Home</a></p>
  </div>
  
  <!--  output list in html-->
  <div id="list">
    <ul>
      <li><a href="detail.php?table=blog">Blog</a></li>
      <li><a href="detail.php?table=users">Users</a></li>
      <li><a href="detail.php?table=library">Library</a></li>
      <li><a href="detail.php?table=comments">Comments</a></li>
    </ul>



  </div>
  <!--  if single book output detail-->
  <div id="detail">
    
    <form id="create_blog" 
              method="post"
              
              name="edit_blog"
              autocomplete="on" novalidate>
      <fieldset>
        <legend>Please provide the following details</legend>
        <p>
          
          <input type="hidden"
            id="id" 
            name="id" 
            />
        </p>
        <p>
          <label for="title">Title</label>
          <input type="text"
            id="title" 
            name="title" 
            size="30"
            placeholder="Title" value="<?php if(!empty($fields['title'])) 
                    echo $v->esc_attr($fields['title']); ?>"
            />
        </p>

        <?php  if(!empty($errors['title'])) : ?>
          <span class="error"><?=$errors['title']?></span>
        <?php endif; ?>

        <p>
          <label for="post_id">Post Id</label>
          <input type="text"
            id="post_id" 
            name="post_id" 
            size="30"
            placeholder="Post Id" value="<?php if(!empty($fields['post_id'])) 
                    echo $v->esc_attr($fields['post_id']); ?>"
            />
        </p>

        <?php  if(!empty($errors['post_id'])) : ?>
          <span class="error"><?=$v->esc_attr($errors['post_id'])?></span>
        <?php endif; ?>

        <p>
          <label for="description">Description</label>
          <input type="text"
            id="description" 
            name="description" 
            size="30"
            placeholder="Description" value="<?php if(!empty($fields['description'])) 
                    echo $v->esc_attr($fields['description']); ?>"
            />
        </p>
        <?php  if(!empty($errors['description'])) : ?>
          <span class="error"><?=$v->esc_attr($errors['description'])?></span>
        <?php endif; ?>
        
        <p>
          <label for="status_check">Status Check</label>
          <select name="status_check">
            <option  <?php if($fields['status_check']=='0'){echo "selected";}?> value="0">No</option>
            <option  <?php if($fields['status_check']=='1'){echo "selected";}?> value="1">Yes</option>
          </select>
        </p>
        <?php  if(!empty($errors['status_check'])) : ?>
          <span class="error"><?=$v->esc_attr($errors['status_check'])?></span>
        <?php endif; ?>

        <p>
          <label for="category_id">Category Id</label>
          <input type="text"
            id="category_id" 
            name="category_id" 
            size="30"
            placeholder="Category Id" value="<?php if(!empty($fields['category_id'])) 
                    echo $v->esc_attr($fields['category_id']); ?>"
            />
        </p>
        <?php  if(!empty($errors['category_id'])) : ?>
          <span class="error"><?=$v->esc_attr($errors['category_id'])?></span>
        <?php endif; ?>

        <p>
          <label for="user_id">User Id</label>
          <input type="text"
            id="user_id" 
            name="user_id" 
            size="30"
            placeholder="User Id" value="<?php if(!empty($fields['user_id'])) 
                    echo $v->esc_attr($fields['user_id']); ?>"
            />
        </p>
        <?php  if(!empty($errors['user_id'])) : ?>
          <span class="error"><?=$v->esc_attr($errors['user_id'])?></span>
        <?php endif; ?>

        <p>
          <label for="post_date">Post Date</label>
          <input type="text"
            id="post_date" 
            name="post_date" 
            size="30"
            placeholder="Post date" value="<?php if(!empty($fields['post_date'])) 
                    echo $v->esc_attr($fields['post_date']); ?>"
            />
        </p>
        <?php  if(!empty($errors['post_date'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['post_date'])?></span>
             <?php endif; ?>

        <p>
          <label for="is_deleted">Is Deleted?</label>
          <select name="is_deleted">
            <option  <?php if($fields['is_deleted']=='0'){echo "selected";}?> value="0">No</option>
            <option  <?php if($fields['is_deleted']=='1'){echo "selected";}?> value="1">Yes</option>
          </select>

          
        </p>
        <?php  if(!empty($errors['is_deleted'])) : ?>
          <span class="error"><?=$v->esc_attr($errors['is_deleted'])?></span>
        <?php endif; ?>



        <p>
          <label for="content">Content</label></p>
        <p>  
          <textarea id="content" 
            name="content" 
            placeholder="Content"
            cols="60" 
            rows="15"><?php if(!empty($fields['content'])) 
                    echo $v->esc_attr($fields['content']); ?></textarea> 
        </p>
        <?php  if(!empty($errors['content'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['content'])?></span>
             <?php endif; ?> 
        <p>
          <input type="submit" value="Update" name="submit" id="submit"/>
          <input type="reset"  value="Reset" name="reset" id="reset"/>
        </p>
          
      </fieldset>
    </form>

  </div>
</body>
</html>