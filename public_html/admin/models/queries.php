<?php
/**
  * Capstone
  * @file queries.php
  * @course  PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-09-12
  */

/**
 * function to get data from the table
 * @param  [object] $dbh       [Database handling object]
 * @param  [String] $tablename [The name of table]
 * @return [Array]   
 */
function getTable($dbh,$tablename)
{
	$table=$tablename;
	if($table='blog'){
		$query = 'SELECT 
		  blog.id AS id,
		  blog.title AS title,
		  users.first_name AS author_f,
		  users.last_name AS author_l,
		  blog.post_date AS post_date,
		  category.name AS category,
		  blog.user_id AS user_id,
		  blog.is_deleted AS is_deleted,
		  blog.updated_at
		  FROM blog
		  JOIN users USING(user_id)
		  JOIN category USING(category_id)
		  ORDER BY id
		  ';
	}

	elseif($table='users'){
		$query = 'SELECT 
		  user_id,
		  first_name,
		  last_name,
		  age,
		  street,
		  city,
		  province,
		  postal_code,
		  country,
		  email,
		  phone,
		  description
		  FROM users
		  
		  ';
	}

	elseif($table='comments'){
		$query = 'SELECT 
		  id,
		  blog_id,
		  user_id,
		  comment,
		  date_posted,
		  reported,
		  deleted
		  FROM
		  comments

		  ';
	}
	$stmt =$dbh-> prepare($query);
		
	$stmt -> execute();
		  
	return  $stmt-> fetchAll(PDO::FETCH_ASSOC);
	
}

/**
 * function to search using keyword
 * @param  [object] $dbh    [Database handling object]
 * @param  [String] $search [the keyword to be searched]
 * @return [Array]
 */
function searchTerms($dbh,$search)
{
	
	$query = "SELECT 
		  blog.id AS id,
		  blog.title AS title,
		  users.first_name AS author_f,
		  users.last_name AS author_l,
		  blog.post_date AS post_date,
		  category.name AS category,
		  blog.user_id AS user_id,
		  blog.is_deleted AS is_deleted,
		  blog.updated_at
		  FROM blog
		  JOIN users USING(user_id)
		  JOIN category USING(category_id)
		  WHERE blog.title LIKE CONCAT('%', :search, '%')";
	
	$stmt =$dbh-> prepare($query);

	 $stmt -> bindValue(':search', $search, PDO::PARAM_STR);
		
	$stmt -> execute();
		  
	return  $stmt-> fetchAll(PDO::FETCH_ASSOC);
}

/**
 * to get aggregate data from blog table
 * @param  [object] $dbh [Database handling object]
 * @return [Array]     
 */
function getBlogData($dbh)
{
	
	$query = "SELECT 
		  
		  count(blog.id) AS id,
		  round(avg(blog.like_count),2) AS likes,
		  max(blog.like_count) AS max,
		  min(blog.like_count) AS min
		  FROM blog
		  JOIN users USING(user_id)
		  JOIN category USING(category_id)
		  ";
	
	$stmt =$dbh-> prepare($query);
	
	$stmt -> execute();
		  
	return  $stmt-> fetch(PDO::FETCH_ASSOC);
}

/**
 * to get aggregate data from users table
 * @param  [object] $dbh [Database handling object]
 * @return [Array]
 */
function getUserData($dbh)
{
	
	$query = "SELECT 
		  
		  count(users.user_id) AS id
		  FROM users
		  
		  ";
	
	$stmt =$dbh-> prepare($query);
	
	$stmt -> execute();
		  
	return  $stmt-> fetch(PDO::FETCH_ASSOC);
}

/**
 * To insert a new record from admin
 * @param  [object] $dbh  [database handling object]
 * @param  [Array] $post [values of $_POST superglobal]
 * @return [Array]
 */
function insertRecord($dbh,$post)
{
	$query = 'INSERT INTO blog 
              (title, post_id, description, content, status_check, category_id, user_id, post_date, is_deleted)
              VALUES
              (:title, :post_id, :description, :content, :status_check, :category_id, :user_id, :post_date, :is_deleted)';

      $stmt = $dbh->prepare($query);

      $params = array (
                ':title' => $post['title'],
                ':post_id'=> $post['post_id'], 
                ':description'=> $post['description'], 
                ':content'=> $post['content'], 
                ':status_check'=> $post['status_check'], 
                ':category_id'=> $post['category_id'], 
                ':user_id'=> $post['user_id'], 
                ':post_date'=> $post['post_date'], 
                ':is_deleted'=> $post['is_deleted']
                
                );
      if($stmt->execute($params)) {
        //if the data is inserted successfully, set the session variables and go to profile page
        $_SESSION['create_success'] = true;
        $_SESSION['create_msg'] = 'Congratulations, you have successfully inserted a blog!!';
        
        header('Location:detail.php?table=blog');
        die;
        } // end if stmt 
}

/**To edit a blog
 * @param  [object] $dbh  [database handling object]
 * @param  [Array] $post [values of $_POST superglobal]
 * @return [Array]
 */
function editBlog($dbh, $post)
{
	$query = 'Update  blog set
                title = :title,
                post_id = :post_id, 
                description = :description, 
                content = :content, 
                status_check = :status_check, 
                category_id = :category_id, 
                user_id = :user_id, 
                post_date  = :post_date, 
                is_deleted = :is_deleted,
                updated_at= current_timestamp()
                WHERE id = :id

              ';

    $stmt = $dbh->prepare($query);

    $params = array (
                ':title' => $post['title'],
                ':post_id'=> $post['post_id'], 
                ':description'=> $post['description'], 
                ':content'=> $post['content'], 
                ':status_check'=> $post['status_check'], 
                ':category_id'=> $post['category_id'], 
                ':user_id'=> $post['user_id'], 
                ':post_date'=> $post['post_date'], 
                ':is_deleted'=> $post['is_deleted'],
                ':id' => $id
                
                );
    if($stmt->execute($params)) {
        //if the data is inserted successfully, set the session variables and go to profile page
        $_SESSION['update_success'] = true;
        $_SESSION['update_msg'] = 'Congratulations, you have successfully Updated a blog!!';
        header('Location:detail.php?table=blog');
        die;
        } // end if stmt 
}

/**
 * To restore the deleted blog
 * @param  [object] $dbh  [database handling object]
 * @param  [Array] $post [values of $_POST superglobal]
 * @return [Array]
 */
function restoreBlog($dbh,$id)
{
	$query = 'Update  blog set
                is_deleted = 0,
                updated_at= current_timestamp()
                WHERE id = :id
              ';

	$stmt = $dbh->prepare($query);

	$params = array (
	          ':id' => $id
	          );
	if($stmt->execute($params)) {
	  //if the data is inserted successfully, set the session variables and go to profile page
	  $_SESSION['restore_success'] = true;
	  $_SESSION['restore_msg'] = 'Congratulations, you have successfully restored a blog!!';
	  header('Location:detail.php?table=blog');
	  die;
	}
}

/**
 * to set the flash messages
 * @param  [Boolean] $session_flag    [The boolean to check if the session is set for the message]
 * @param  [String] $session_message [The message to be passed]
 * @return [String]      
 */
function flashMessage($session_flag, $session_message)
{
	
  if(isset($_SESSION[$session_flag])) {
	  if(isset($_SESSION[$session_message])){
	    //if the message is set, display it and unset it so that it dissapears on updating
	    $msg=$_SESSION[$session_message];
	    
	    unset($_SESSION[$session_message]);
	    unset($_SESSION[$session_flag]);
	    session_regenerate_id();
	    return $msg;
	   }
	}
}

/**
 * gets the values of fields to make edit form sticky in the beginning
 * @param  [object] $dbh [database handling object]
 * @param  [Number] $id  [Id of blog]
 * @return [Array]      
 */
function getFields($dbh,$id)
{
  $query = 'SELECT  
                title ,
                post_id , 
                description , 
                content, 
                status_check, 
                category_id, 
                user_id , 
                post_date , 
                is_deleted 
                FROM blog
                WHERE id = :id

              ';

    $stmt = $dbh->prepare($query);

    $params = array (
                ':id' => $id
                );
    $stmt->execute($params);
    return  $stmt-> fetch(PDO::FETCH_ASSOC);
}