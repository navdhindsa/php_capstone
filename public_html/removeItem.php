<?php 
/**
  * Capstone
  * @file removeItem.php
  * @course  PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

$title = 'Post';
$slug = 'post';
 
require __DIR__ . '/../config.php';
require '../includes/connect_db.inc.php';

include __DIR__ . '/../database/queries.php';

$id= $_GET['id'];

unset($_SESSION['cart'][$id]);

$_SESSION['removeItem']=true;
$_SESSION['removeItemMsg']='You have successfully removed an item from cart!!';

header("Location: {$_SERVER['HTTP_REFERER']}");

die;