<?php 
/**
  * Capstone
  * @file post.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

$title = 'Post';
$slug = 'post';
 
require __DIR__ . '/../config.php';
require '../includes/connect_db.inc.php';

include __DIR__ . '/../database/queries.php';



if(!isset($_SESSION['id'])){
	$_SESSION['logout']=true;
    $_SESSION['logout_msg']='You need to login to save items to library!';

//going to the login page
header('Location:login.php');
die;

}

$id=$_SESSION['id'];

foreach($_SESSION['cart'] AS $row){
  addToLibrary($dbh, $row['id'], $id);
}


unset($_SESSION['cart']);

header('Location:library.php');
die;
