<?php 
/**
  * Capstone
  * @file addToCart.php
  * @course  PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

$title = 'Post';
$slug = 'post';
 
require __DIR__ . '/../config.php';
require '../includes/connect_db.inc.php';

include __DIR__ . '/../database/queries.php';


$id= $_GET['post_id'];

$details= getPostDetail($dbh,$id);





$item= [
	'id'=>$details['blog_id'],
  'title'=> $details['title'],
  'author' => $details['author_f'] .' ' . $details['author_l'],
  'category'=>$details['category'],
  'image' =>$details['image']

];

$_SESSION['cart'][$id] = $item;


$_SESSION['addItem']=true;
$_SESSION['addItemMsg']='You have successfully added an item to cart!!';

header("Location: {$_SERVER['HTTP_REFERER']}");
die;

