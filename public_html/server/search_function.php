<?php

/**
  * Capstone
  * @file search_function.php
  * @course  PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-09-11
  */

//if the server request method is post
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
   
    // get the value to be searched
    $keyword = $_POST['search'];
    
    //if the value is not empty
    if(!empty($keyword)){

      //creating database attributes
      $dbh = new PDO('mysql:host=localhost;dbname=capstone', 'root', 'root');
      $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


      //the query to search
  		$query = "SELECT blog.title AS title, users.first_name AS author_f, users.last_name AS author_l, category.name AS category, blog.id AS blog_id, blog.user_id AS user_id, blog.created_at AS year, blog.thumb AS thumb FROM blog JOIN users USING(user_id) JOIN category USING(category_id) WHERE blog.title LIKE CONCAT(:keyword, '%')";
  		$stmt = $dbh -> prepare($query);
      $stmt -> bindValue(':keyword', $keyword, PDO::PARAM_STR);
  		$stmt -> execute();

  		$blogs = $stmt -> fetchAll(PDO::FETCH_ASSOC);
  		
      //sending the content in json encoded formatted
      header('Content-type: application/json');
      echo json_encode($blogs);
    }

  }