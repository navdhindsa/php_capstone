<?php 
/**
  * Capstone
  * @file register.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

$title = 'Register';
$slug = 'register';

//including the required files
require '../config.php';
require '../includes/connect_db.inc.php';


//including the validator class
use \Classes\Utilities\Validator;
$v = new Validator();

//if the user has submitted data, check for errors from validation class
if($_SERVER['REQUEST_METHOD'] == 'POST') {
//csrf checking
    if($_POST['csrf'] != $_SESSION['csrf']){
      die('You have not submitted the form from our website!');

    }//end csrf checking
  
  $v->required('first_name');
  $v->required('last_name');
  $v->required('street');
  $v->required('city');
  $v->required('postal_code');
  $v->required('province');
  $v->required('country');
  $v->required('phone');
  $v->required('comment');
  $v->required('email');
  $v->required('age');
  $v->required('password');
  $v->required('confirm_password');
  $v->validEmail('email');
  $v->passwordMatch();
  $v->validAge('age');
  $v->validPhone('phone');
  $v->validPostCode('postal_code');
  $v->validString('first_name');
  $v->validString('last_name');
  $v->strongPass('password'); 
  $v->minLength('age',5);
  $v->maxLength('age',105);

  //make a list of all errors in an array
  $errors = $v->errors();

  //if there are no errors, insert into users table in the try block
  if(empty($errors )) {
    try{
      $query = 'INSERT INTO users 
              (first_name, last_name, email, age, password, street, city, province, country, postal_code, phone)
              VALUES
              (:first_name, :last_name, :email, :age, :password, :street, :city, :province, :country,   :postal_code, :phone)';

      $stmt = $dbh->prepare($query);

      $params = array (
                ':first_name' => $_POST['first_name'],
                ':last_name' => $_POST['last_name'],
                ':email' => $_POST['email'],
                ':age' => $_POST['age'],
                ':password' => password_hash($_POST['password'],PASSWORD_DEFAULT),
                ':street' =>$_POST['street'], 
                ':city'=>$_POST['city'], 
                ':province' =>$_POST['province'], 
                ':country'=>$_POST['country'], 
                ':phone'=>$_POST['phone'],
                ':postal_code'=>$_POST['postal_code']
                );
      if($stmt->execute($params)) {
        //if the data is inserted successfully, set the session variables and go to profile page
        $_SESSION['id'] = $dbh->lastInsertId();
        $_SESSION['logged_in'] = true;
        $success = true;
        $_SESSION['msg'] = "Welcome to your profile {$users['first_name']}";
        header('Location:profile.php');
        die;
        } // end if stmt 
    }//end try block

    //if there is an exception for not having a unique email, 
    //then catch it and give the required error message
    catch(PDOException $e){
      if(!empty($e->getMessage())){
        $errors['email']='This email is already taken, did you forget your password??';
      }
    }
  } // end if no errors
} // end if POST submission

//include the header file
include '../includes/header.inc.php'; 
?>
  <body id="connect">
   <?php include '../includes/nav.inc.php' ?>    
          <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->
<?php if(empty($success)) : ?>
        <h2>Get in touch. </h2>
        <form id="email" class="formEl"
              method="post"
              action="register.php"
              name="email"
              autocomplete="on" novalidate>
          <fieldset>
            <legend>Please provide the following details</legend>
            <input type="hidden" name="csrf" value="<?=$csrf?>" />
            <p>
              <label for="first_name">First Name </label>
              <input type="text"
                id="first_name" 
                name="first_name" 
                maxlength="25"
                size="30"
                placeholder="First Name" value="<?php if(!empty($_POST['first_name'])) 
                    echo $v->esc_attr($_POST['first_name']); ?>"
                />
             </p>
             <?php  if(!empty($errors['first_name'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['first_name'])?></span>
             <?php endif; ?>
            
            <p>
              <label for="last_name">Last Name </label>
              <input type="text"
                id="last_name" 
                name="last_name" 
                maxlength="25"
                size="30"
                placeholder="Last Name" value="<?php if(!empty($_POST['last_name'])) 
                    echo $v->esc_attr($_POST['last_name']); ?>"
                />
            </p>
            <?php if(!empty($errors['last_name'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['last_name'])?></span>
             <?php endif; ?>
            
            
            
            <p >
            <label  for="age">Age</label>
            <input type="text" name="age" maxlength="255" placeholder="Age"
            value="<?php if(!empty($_POST['age'])) 
                    echo $v->esc_attr($_POST['age']); ?>" />
            
        </p>
            <?php if(!empty($errors['age'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['age'])?></span>
             <?php endif; ?>
            
            
            <p>
              <label for="street">Street Address </label>
              <input type="text"
                id="street" 
                name="street" 
                maxlength="25"
                size="30"
                placeholder="Street Address" value="<?php if(!empty($_POST['street'])) 
                    echo $v->esc_attr($_POST['street']); ?>"
                />
            </p>
            <?php if(!empty($errors['street'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['street'])?></span>
             <?php endif; ?>
            <p>
              <label for="city">City</label>
              <input type="text"
                id="city" 
                name="city" 
                maxlength="25"
                size="30"
                placeholder="City" value="<?php if(!empty($_POST['city'])) 
                    echo $v->esc_attr($_POST['city']); ?>"
                />
            </p>
            <?php if(!empty($errors['city'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['city'])?></span>
             <?php endif; ?>
            <p>
              <label for="province">Province</label>
              <input type="text"
                id="province" 
                name="province" 
                maxlength="25"
                size="30"
                placeholder="Province" value="<?php if(!empty($_POST['province'])) 
                    echo $v->esc_attr($_POST['province']); ?>"
                />
            </p>
            <?php if(!empty($errors['province'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['province'])?></span>
             <?php endif; ?>
            <p>
              <label for="postal_code">Postal Code</label>
              <input type="text"
                id="postal_code" 
                name="postal_code" 
                maxlength="25"
                size="30"
                placeholder="A1A 1A1" value="<?php if(!empty($_POST['postal_code'])) 
                    echo $v->esc_attr($_POST['postal_code']); ?>"
                />
            </p>
            <?php if(!empty($errors['postal_code'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['postal_code'])?></span>
             <?php endif; ?>
            
            <p>
              <label for="country">Country</label>
              <input type="text"
                id="country" 
                name="country" 
                maxlength="25"
                size="30"
                placeholder="Country" value="<?php if(!empty($_POST['country'])) 
                    echo $v->esc_attr($_POST['country']); ?>"
                />
            </p>
            <?php if(!empty($errors['country'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['country'])?></span>
             <?php endif; ?>
            
            <p>
              <label for="email">Email Address </label> 
              <input type="email" 
                     name="email" 
                     id="email"
                     placeholder="abc@example.com" value="<?php if(!empty($_POST['email'])) 
                    echo $v->esc_attr($_POST['email']); ?>"
                     />
            </p>
            <?php if(!empty($errors['email'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['email'])?></span>
             <?php endif; ?>
           
            <p>
            <label for="phone">Telephone </label>
              <input type="text"
                     id="phone"
                     name="phone"
                     maxlength="18"
                     placeholder="xxx xxx xxxx" value="<?php if(!empty($_POST['phone'])) 
                    echo $v->esc_attr($_POST['phone']); ?>"
                      />
            </p>
              <?php if(!empty($errors['phone'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['phone'])?></span>
             <?php endif; ?>
               
            <p>
              <label for="password">Password</label>
              <input type="password"
                id="password" 
                name="password" 
                maxlength="25"
                size="30"
                placeholder="Password" 
                />
            </p>
            <?php if(!empty($errors['password'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['password'])?></span>
             <?php endif; ?>
       
            <p>
              <label for="confirm_password">Confirm Password</label>
              <input type="password"
                id="confirm_password" 
                name="confirm_password" 
                maxlength="25"
                size="30"
                placeholder="Confirm Password"
                />
            </p>
            <?php if(!empty($errors['confirm_password'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['confirm_password'])?></span>
             <?php endif; ?>
            <p>
              <label for="comment">Comments</label>  <br />
              <textarea cols="60" 
                        rows="6" 
                        id="comment" 
                        name="comment" value="<?php if(!empty($_POST['comment'])) 
                    echo $v->esc_attr($_POST['comment']); ?>">
              </textarea>
            </p>
            <?php if(!empty($errors['comment'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['comment'])?></span>
             <?php endif; ?>
            <p>
              <input type="submit" 
                     name="submit" 
                     id="submit" 
                     value="Submit" 
              />
              &nbsp;&nbsp;
              <input type="reset" 
                     name="reset" 
                     id="reset" 
                     value="Reset" 
              />
              </p>

          </fieldset>
        </form>
    <?php else : ?>

    <h2>Thankyou for registering!</h2>

    <p>You have submitted the following Information:</p>

    <p>
        <strong>First Name</strong>: <?=$v->esc($users['first_name'])?><br />
        <strong>Last Name</strong>: <?=$v->esc($users['last_name'])?><br />
        <strong>Email</strong>: <?=$v->esc($users['email'])?><br />
      <strong>Phone</strong>: <?=$v->esc($users['phone'])?><br />
        <strong>Age</strong>: <?=$v->esc($users['age'])?><br />
            <strong>Street</strong>: <?=$v->esc($users['street'])?><br />
            <strong>City</strong>: <?=$v->esc($users['city'])?><br />
            <strong>Postal Code</strong>: <?=$v->esc($users['postal_code'])?><br />
            <strong>Province</strong>: <?=$v->esc($users['province'])?><br />
            <strong>Country</strong>: <?=$v->esc($users['country'])?><br />
            
            <strong>Comment</strong>: <?=$v->esc($users['comment'])?><br />
            </p>

    <p>Back to <a href="connect.php">Registration Form</a></p>

  
    <?php endif; ?>

      </div>
      <?php include '../includes/footer.inc.php' ?>

