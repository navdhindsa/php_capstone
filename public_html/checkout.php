<?php 
/**
  * Capstone
  * @file post.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

$title = 'Post';
$slug = 'post';
 
require __DIR__ . '/../config.php';
require '../includes/connect_db.inc.php';

include __DIR__ . '/../database/queries.php';




$dets=$_SESSION['cart'];

if(empty($dets)){
  $msg="You do not have anything to be saved yet, please select some items to be saved!";
}

include '../includes/header.inc.php'; 
  
?>
  <body id="post">
   <?php include '../includes/nav.inc.php' ?>    
    
      <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->

        <!-- Main content starts-->  
      
        
        
       

<div id="home_content">

<?php if(empty($dets)) : ?>
  <h1><?=$msg;?></h1>


<?php else : ?>
  <h1>You have following items to be saved:  </h1>  
    <div id="checkout">
    	
    <table>
      <tr>
      	
      	<th>Title</th>
      	<th>Author</th>
      	<th>Category</th>
      </tr>

    <?php foreach($dets AS $row) : ?>
      <tr>
      	
      	<td><?=$row['title']?></td>
      	<td><?=$row['author']?></td>
      	<td><?=$row['category']?></td>
      </tr>
    <?php endforeach; ?>
    </table>

    <div class="outer_a">
    	<a class="button" href="confirm.php?>">Add To Library</a>
    	<a class="button" href="posts.php">Go Back</a>



    </div>
               


    </div>        
            
            
  <?php endif; ?>
        
      
      </div>

    <?php include '../includes/footer.inc.php' ?>

