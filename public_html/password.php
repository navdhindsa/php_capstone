<?php 
/**
  * Capstone
  * @file password.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

$title = 'Change Password';
$slug = 'profile';

//including the required files
require '../config.php';
require '../includes/connect_db.inc.php';
require '../database/queries.php';


//including the validator class
use \Classes\Utilities\Validator;
$v = new Validator();

//get the id from the session variable
$id = $_SESSION['id'];





//if the user has submitted data, check for errors from validation class
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  //csrf checking
  if($_POST['csrf'] != $_SESSION['csrf']){
      die('You have not submitted the form from our website!');

  }//end csrf checking
  $fields=$_POST;
  $v->required('password');
  $v->required('old_password');
  $v->required('confirm_password');
  $v->required('email');

  $v->validEmail('email');
  $v->passwordMatch();
  $v->strongPass('password'); 

  //make a list of all errors in an array
  $errors = $v->errors();
  
  //if there are no errors, insert into users table in the try block
  
  if(empty($errors )) {

      if(
      	editPassword($dbh, $_POST,$id)){
        header('Location:profile.php');
      }
 

    //if there is an exception for not having a unique email, 
    
   
  } // end if no errors
} // end if POST submission

//include the header file
include '../includes/header.inc.php'; 
?>
  <body id="connect">
   <?php include '../includes/nav.inc.php' ?>    
          <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->
<?php if(empty($success)) : ?>
        <h2>Change your password. </h2>
        <form id="email" class="formEl"
              method="post"
              action="password.php"
              name="email"
              autocomplete="on" novalidate>
          <fieldset>
            <legend>Please fill the following details</legend>
            <input type="hidden" name="csrf" value="<?=$_SESSION['csrf']?>" />
            
            <p>
              <label for="email">Email Address </label> 
              <input type="email" 
                     name="email" 
                     id="email"
                     placeholder="abc@example.com" value="<?php if(!empty($fields['email'])) 
                    echo $v->esc_attr($fields['email']); ?>"
                     />
            </p>
            <?php if(!empty($errors['email'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['email'])?></span>
             <?php endif; ?>
            
            <p>
              <label for="old_password">Current Password</label>
              <input type="password"
                id="old_password" 
                name="old_password" 
                maxlength="25"
                size="30"
                placeholder="Old Password" 
                />
            </p>
            <?php if(!empty($errors['old_password'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['old_password'])?></span>
             <?php endif; ?>
			
             <p>
              <label for="password">New Password</label>
              <input type="password"
                id="password" 
                name="password" 
                maxlength="25"
                size="30"
                placeholder="New Password" 
                />
            </p>
            <?php if(!empty($errors['password'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['password'])?></span>
             <?php endif; ?>
       

            <p>
              <label for="confirm_password">Confirm Password</label>
              <input type="password"
                id="confirm_password" 
                name="confirm_password" 
                maxlength="25"
                size="30"
                placeholder="Confirm Password"
                />
            </p>
            <?php if(!empty($errors['confirm_password'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['confirm_password'])?></span>
             <?php endif; ?>
            <p>
              <input type="submit" 
                     name="submit" 
                     id="submit" 
                     value="Submit" 
              />
              &nbsp;&nbsp;
              <input type="reset" 
                     name="reset" 
                     id="reset" 
                     value="Reset" 
              />
              </p>

          </fieldset>
        </form>

        
    <?php else : ?>

    <h2>Thankyou for registering!</h2>

    <p>You have submitted the following Information:</p>

    
    <p>Back to <a href="connect.php">Registration Form</a></p>

  
    <?php endif; ?>

      </div>
      <?php include '../includes/footer.inc.php' ?>

