<?php 
/**
  * Capstone
  * @file post.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

$title = 'Post';
$slug = 'post';
 
require __DIR__ . '/../config.php';
require '../includes/connect_db.inc.php';

include __DIR__ . '/../database/queries.php';

use \Classes\Utilities\Validator;
$v= new Validator();


//if the person has removed item from cart, check for the session variable
if(isset($_SESSION['removeItem'])) {
  if(isset($_SESSION['removeItemMsg'])){
    //if the message is set, display it and unset it so that it dissapears on updating
    $msg=$_SESSION['removeItemMsg'];
    unset($_SESSION['removeItemMsg']);
    unset($_SESSION['removeItem']);
    session_regenerate_id();
   }
}

//if the person has removed item from cart, check for the session variable
if(isset($_SESSION['addItem'])) {
  if(isset($_SESSION['addItemMsg'])){
    //if the message is set, display it and unset it so that it dissapears on updating
    $msg=$_SESSION['addItemMsg'];
    unset($_SESSION['addItemMsg']);
    unset($_SESSION['addItem']);
    session_regenerate_id();
   }
}

if(!isset($_SESSION['cart'])){
$_SESSION['cart'] = array();
}
$cat= getCats($dbh);

if(!empty($_GET['post_id'])){  
  $post_id=$_GET['post_id'];  
  $posts = getPostDetail($dbh,$post_id);
  $author_id = $posts['user_id'];
}

//var_dump($posts);

include '../includes/header.inc.php'; 
  
?>
  <body id="post">
   <?php include '../includes/nav.inc.php' ?>    
    
      <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->

        <!-- Main content starts-->  
        <div id="hero">
          <?php if($title == "Post"){
            include '../includes/search.inc.php';
          } ?>
          <!--
          <div class="wrap">
            <div class="search">
              <form id="search_form" action="" method="" autocomplete="off" novalidate >
                <input id="s" type="text"  name="s" class="searchTerm" placeholder="What are you looking for?">
                <button type="submit" value="search" class="searchButton">&#128269;</button>
              </form>
            </div>
          </div>-->
        </div>
        
        
        <?php if(isset($msg)) : ?>

            <div class="success"><?=$v->esc_attr($msg);?></div>
            <?php endif; ?>

<div id="home_content">
          
        <div class="categories">

		      <h3>Categories</h3>
		      <ul id="cat_ul">
			    <?php foreach($cat AS $row) : ?>
         
			      <li><a href="posts.php?category_id=<?=$v->esc_attr($row['cat_id']);?>"><?=$v->esc_attr($row['name']);?></a></li>
		
          <?php endforeach;?>
		      </ul>

          <?php if(!empty($_SESSION['cart'])) : ?>
          <div class="cart_box">
           <p>Your Cart:</p>
            <ul>
              
              <?php foreach($_SESSION['cart'] AS $row) : ?>
                <li>
                  
                  <?=$v->esc_attr($row['title']);;?> &nbsp; &nbsp;&nbsp;
                  
                  <a href="removeItem.php?id=<?=$v->esc_attr($row['id']);?>">X</a>
                </li>

              <?php endforeach; ?>

            </ul>
            <a  href="checkout.php">Checkout</a>
          </div>

        <?php endif; ?>
          <div id="details">
            <div id="image">
            </div>
            <h1><?=$v->esc_attr($posts['title']);;?></h1>
            <h2><?=$v->esc_attr($posts['category']);;?></h2>
            <h3>By: <?=$v->esc_attr($posts['author_f']) . " " .$v->esc_attr($posts['author_l']);?></h3>
            <img src="images/posts/<?=$v->esc_attr($posts['image']);?>.jpg" width="850" height="400" alt="<?=$v->esc_attr($posts['title']);?>" />
            <p id="mypara"><a class="button button3" href="addToCart.php?post_id=<?=$v->esc_attr($_GET['post_id'])?>">Save</a></p>
            <p><?=nl2br($v->esc($posts['content']));?></p>

          </div>
          <hr />          
	      </div><!-- categories div ends -->
        
        <div id="lower">
          <span id="circle">
            <img style="border-radius:50%" src="images/users/<?=$posts['u_img']?>.jpg" width="100" height="100" alt="<?=$v->esc_attr($posts['title']);;?>" />

          </span>
          <p><span><?=$v->esc_attr($posts['author_f']) . " " .$v->esc_attr($posts['author_l'])?></span></p>
        </div>
        <hr>
        
        <div id="comments">
          Comments- There are no comments yet!!     
        </div>
      
      </div>

    <?php include '../includes/footer.inc.php' ?>

