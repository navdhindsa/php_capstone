
window.onscroll = function() {myFunction()};

function myFunction() {
  if(document.body.scrollTop > 1 || document.documentElement.scrollTop > 1) {
    document.getElementById("nav").className = "inner_nav";
    document.getElementsByClassName("nav2").className = "inner_nav2";
  } 
  else{
    document.getElementById("nav").className = "";
    document.getElementsByClassName("nav2").className = "";
  }
}
//profile
function profileChanger()
{
	document.getElementById('profile').src="images/nav_profile_hover.svg";
}

function profileOriginal()
{	
	document.getElementById('profile').src="images/nav_profile.svg";
}
//login
function loginChanger()
{
	document.getElementById('login').src="images/nav_login_hover.svg";
}

function loginOriginal()
{	
	document.getElementById('login').src="images/nav_login.svg";
}
//logout
function loChanger()
{
	document.getElementById('logout').src="images/nav_logout_hover.svg";
}

function loOriginal()
{	
	document.getElementById('logout').src="images/nav_logout.svg";
}

//register
function regChanger()
{
	document.getElementById('reg').src="images/nav_register_hover.svg";
}

function regOriginal()
{	
	document.getElementById('reg').src="images/nav_register.svg";
}

//fb
function fbChanger()
{
	document.getElementById('fb').src="images/facebooks_icon_hover.svg";
}

function fbOriginal()
{	
	document.getElementById('fb').src="images/facebooks_icon.svg";
}

//ig
function igChanger()
{
	document.getElementById('ig').src="images/instagram_icon_hover.svg";
}

function igOriginal()
{	
	document.getElementById('ig').src="images/instagram_icon.svg";
}

//tw
function twChanger()
{
	document.getElementById('tw').src="images/twitter_icon_hover.svg";
}

function twOriginal()
{	
	document.getElementById('tw').src="images/twitter_icon.svg";
}

//linkedin
function lkChanger()
{
	document.getElementById('lk').src="images/linkedin_icon_hover.svg";
}

function lkOriginal()
{	
	document.getElementById('lk').src="images/linkedin_icon.svg";
}

//admin
function adminChanger()
{
	document.getElementById('admin').src="images/admin_hover.svg";
}

function adminOriginal()
{	
	document.getElementById('admin').src="images/admin.svg";
}

//library
function libraryChanger()
{
	document.getElementById('library').src="images/library_hover.svg";
}

function libraryOriginal()
{	
	document.getElementById('library').src="images/library.svg";
}

//cart
function cartChanger()
{
	document.getElementById('cart').src="images/cart_hover.svg";
}

function cartOriginal()
{	
	document.getElementById('cart').src="images/cart.svg";
}