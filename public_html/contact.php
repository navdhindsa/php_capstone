<?php 
 /**
  * Capstone
  * @file connect.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

  $title = 'Connect with Us';
  $slug = 'contact';

  //required files
  require '../config.php';
  require '../includes/connect_db.inc.php';
  
//including the validator class
use \Classes\Utilities\Validator;
$v = new Validator();
  //if the server request method is post
  if($_SERVER['REQUEST_METHOD'] == 'POST') {  

    //csrf checking
    if($_POST['csrf'] != $_SESSION['csrf']){
      die('You have not submitted the form from our website!');

    }//end csrf checking
     $v->required('name');
     $v->required('email');
     $v->required('subject');
     $v->required('comment');

     //make a list of all errors in an array
  $errors = $v->errors();

  //if there are no errors, insert into contact table in the try block
  if(empty($errors )) {
      //creating the new PDO object
      $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      //query to insert into table
      $query = 'INSERT INTO contact 
                  (name, email, subject, comment)
                  VALUES
                  (:name,  :email, :subject, :comment)';

      $stmt = $dbh->prepare($query);

      $params = array (
              ':name' => $_POST['name'],
              ':subject' => $_POST['subject'],
              ':email' => $_POST['email'],
              ':comment'=>$_POST['comment']        
          );


      if($stmt->execute($params)) {

       $msg="Thankyou for your message, we will get in touch with you!!";
       
        $success = true;

      } // end if stmt 

     }//end no errors
    
  } // end if POST submission

//including the header file
include '../includes/header.inc.php'; 
?>
  <body id="contact">
   <?php include '../includes/nav.inc.php' ?>    
    <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->
      <?php if(empty($success)) : ?>
        <h2>Please fill out the following form and we will get in touch with you! </h2>
        <form id="contact" class="formEl"
              method="post"
              action="contact.php"
              name="contact"
              autocomplete="on" novalidate>
          <fieldset>
            <legend>Contact Us</legend>
            <input type="hidden" name="csrf" value="<?=$_SESSION['csrf']?>" />
            <p>
              <label for="name">Name </label>
              <input type="text"
                id="name" 
                name="name" 
                maxlength="25"
                size="30"
                placeholder="Name" value="<?php if(!empty($_POST['name'])) 
                    echo esc_attr($_POST['name']); ?>"
                />
            </p>
            <?php  if(!empty($errors['name'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['name'])?></span>
             <?php endif; ?>
            
            <p>
              <label for="email">Email Address </label> 
              <input type="email" 
                     name="email" 
                     placeholder="Email"
                     id="email" value="<?php if(!empty($_POST['email'])) 
                    echo esc_attr($_POST['email']); ?>"
                     />
            </p>
            <?php if(!empty($errors['email'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['email'])?></span>
             <?php endif; ?>
             <p>
              <label for="subject">Subject </label> 
              <input type="text" 
                     name="Subject" 
                     placeholder="subject"
                     id="subject" value="<?php if(!empty($_POST['subject'])) 
                    echo esc_attr($_POST['subject']); ?>"
                     />
            </p>
            <?php if(!empty($errors['subject'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['subject'])?></span>
             <?php endif; ?>
            
            <p>
              <label for="comment">Please leave your comments</label>  <br />
              <textarea cols="30" 
                        rows="6" 
                        id="comment" 
                        name="comment" ><?php if(!empty($_POST['comment'])) 
                    echo esc_attr($_POST['comment']); ?></textarea>
            </p>
            <?php if(!empty($errors['comment'])) : ?>
            <span class="error"><?=$v->esc_attr($errors['comment'])?></span>
             <?php endif; ?>
            <p>
              <input type="submit" 
                     name="submit" 
                     id="submit" 
                     value="Submit" 
              />
              &nbsp;&nbsp;
              <input type="reset" 
                     name="reset" 
                     id="reset" 
                     value="Reset" 
              />
              </p>

          </fieldset>
        </form>
   
      <?php else : ?>

        <h2><?=$msg?></h2>
        
      
    <?php endif; ?>

      </div>
      <?php include '../includes/footer.inc.php' ?>

