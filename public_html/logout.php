<?php 
/**
  * Capstone
  * @file logout.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

//including the required files
require __DIR__ . '/../config.php';
require '../includes/connect_db.inc.php';


//unset the session 
unset($_SESSION);

//destroy the session
session_destroy();

//regenerate the id cuz privilege changes
session_regenerate_id();

//starting the session
session_start();

//setting the session variables to display logout flash messages
$_SESSION['logout']=true;
$_SESSION['logout_msg']='You have successfully logged out!';

//going to the login page
header('Location:login.php');
die;
