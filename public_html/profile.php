<?php 
/**
  * Capstone
  * @file profile.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

$title = 'Profile';
$slug = 'profile';

//including the required files
require __DIR__ . '/../config.php';
require '../includes/connect_db.inc.php';
require '../database/queries.php';

use \Classes\Utilities\Validator;
$v= new Validator();

//get the id from the session variable
$id = $_SESSION['id'];
  // call the function to get details of user
  $users= getProfile($dbh,$id);

 

//if the session variable for logged in is set display the flash message and unset the variable
if(isset($_SESSION['logged_in'])) {
	

    if(isset($_SESSION['admin_msg'])){
      $msg = "<p>" . $_SESSION['msg'] . "</p><p>" .  $_SESSION['admin_msg']. "</p>";
      unset($_SESSION['admin_msg']);
      unset($_SESSION['msg']);
      session_regenerate_id();
    }

    elseif(isset($_SESSION['msg'])){
      $msg=$_SESSION['msg'];
      unset($_SESSION['msg']);
      session_regenerate_id();
    }
    
    if(isset($_SESSION['profile_update_msg'])){
      
      $msg=$_SESSION['profile_update_msg'];
      unset($_SESSION['profile_update_msg']);
      unset($_SESSION['profile_update_success']);
      session_regenerate_id();
    }

}



 


//including the header
include '../includes/header.inc.php';
?>
<body id="profile">
   <?php include '../includes/nav.inc.php' ?>    
          <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->


	<h1>Profile</h1>
          <?php if(isset($msg)) : ?>

            <div class="success"><?=$msg?></div>
            <?php endif; ?>

<div id="home_content">
          
          <div class="home_cat home_cat2 " >
            <div class="home_img home_img2"><img src="images/nav_profile_hover.svg" alt="Profile" title="Profile" />
            </div>
            <h4><?=$v->esc_attr($users['first_name']);?></h4>
            
            
            <button class="button button2">Edit Photo</button>
            
          </div>
  <div id="profile_info">
   <div id="table">
        
        <table >
          <caption>
            This is how much we know you!
          </caption>
          <tbody>
            <?php foreach($users as $key=>$value) : ?>
            <tr>
            
            <th><?=$v->esc_attr(str_replace('_',' ', ucfirst($key)))?></th>
              <td><?=$v->esc_attr($value)?></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
        </table>

      </div><!--Table ends-->
    <span class="button_container">
      <a href="logout.php"><button type="button" class="button" >LOGOUT</button></a>
      <a href="edit_profile.php"><button type="button" class="button" >EDIT</button></a>
      <a href="password.php"><button style="width:90px;" type="button" class="button" >CHANGE PASSWORD</button></a>
    </span>
  </div>
  </div>
	
 </div>
      <?php
  include '../includes/footer.inc.php'; ?>