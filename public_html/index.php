<?php 
 /**
  * Capstone
  * @file index.php
  * @course Intermediate PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */
$title = 'Navdeep Dhindsa - Official Website';
$slug = 'index';

//require the config file 
require __DIR__ . '/../config.php';

//including the validator class
use \Classes\Utilities\Validator;
$v= new Validator();

//including the header file
include '../includes/header.inc.php';
?>

  <body id="home">
    
   <?php include '../includes/nav.inc.php' ?>
      <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->

        <div id="hero">
          
          <h2>Knowledge for Life!</h2>
          <h3>Read on!</h3>
        </div> 
        <div id="home_content">
          
          <p>When your blog speaks to your readers something magical happens. </p>
          <p>You start generating comments on your blog and on Facebook. </p>
          <p>Other bloggers in your industry reach out to you and want to network with you.</p> 
          <p>Your readers start to link to your site on their posts wanting to tell their audience about you.</p> 
          <p>They’re the first to grab your latest content upgrade and they are one of the firsts to read your email or read your latest blog post. </p>
          <p>In effect, they are your loyal audience. </p>
          <p>Many bloggers use their Facebook group as a way to build their tribe, or their email list. </p>
          <p>The main reason bloggers build up their email list or have thousands of Facebook groupies is to market to them. </p>
          <p>Whether it’s a free webinar or a masterclass or even a new product, bloggers who have built a base of loyal and raving fans can profit from their audience.
         </p>
          
      
        </div>
      </div>
      
         <?php include '../includes/footer.inc.php' ?>
      

